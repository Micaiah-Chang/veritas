Readme
======

Veritas is an open source tool for TrueBeam Developer Mode provided by Varian Medical Systems, Palo Alto.
It lets users generate XML beams without assuming any prior knowledge of the underlying XML-schema rules.
This version is based on the schema for TrueBeam 2.0.

For questions, please send us an email at: TrueBeamDeveloper@varian.com

News
====
5/11/2015

Version 2.2 released.

Latest Features
===============
* Imaging points now are saved automatically upon navigation
* Now user can create a mock TrueBeam point from scratch on the imaging modal
* Automatically ensures that a user defines both start and stop points for continuous imaging
* User can now plot any two XML elements against each other, instead of merely MU and

Main functionalities in version 2.2 are as follows
==================================================
* **Create** a new XML beam.
* **Modify** an existing XML beam
* **Convert** DICOM-RT to an XML.
* **Insert imaging points** (kV and MV) in an XML beam.

Bug Fixes
=========
5/11/2015

* MV imaging points no longer produces buggy and invalid XML schema
* Previous button on imaging modal now works

10/24/2014

* Added a factor of 10 to convert MLC values from mm to cm.

Project Details
===============

:Code:            https://bitbucket.org/varianveritas/veritas/
:Issue tracker:   https://bitbucket.org/varianveritas/veritas/issues/
:Documentation:   http://varianveritas.readthedocs.org/en/latest/
:License:         Varian Open Source 1.0

Warning
=======
**Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans**
