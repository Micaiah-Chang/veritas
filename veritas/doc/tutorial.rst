Tutorial
========

This tutorial starts with a brief descrition of the main GUI window and some of the key operations
that can performed. Following that a simple case of how to creat a new XML beam, how to modify an existing
XML beam, insert an imaging point and how to create an XML beam from a DICOM-RT file.

Veritas workflow
----------------

Current version of veritas has two main functions. It lets the user enter a) a new control point and b) add imaging points
to the XML beam. A flowchart showing two central functions is shown in Figure 1.

.. figure:: images/VeritasFlowchart.png

   Figure1: Flowchart showing a typical veritas workflow

The main window
---------------
The main window of the GUI looks as follows:

.. figure:: images/mainWindow2.png
   :scale: 70 %

   Figure (2) The main window of varian veritas

Some of the **key icons** and their functions are in the following section.

**Push Buttons**

- Beam ON : Guides a users through a series of steps for creating a XML beam from scratch.
- Imaging : Lets user insert kV or MV imaging points for a given XML beam.

**Menu Icons**

- New: Same as Beam ON.
- Open: Lets user open an existing XML file and modify control points.
- dc2xml: Convert a given DICOM-RT file to an XML beam.

--------------------------------------------------------------------------------------------------------

Creating a new XML beam
-----------------------

**1) The first control point:**
The first control point some extra info needs to be added.

.. figure:: images/headerinfo.png
   :scale: 75 %

   Figure (3) Insert energy, MLC model, dose rate and other informations

**2) Control point and various axes:**
Enter rest of the values for first control point and add other control points

.. figure:: images/controlpoint2.png
   :scale: 75 %

   Figure (4) Insert values corresponding to various TrueBeam axes

.. Note::
   **Control point navigation** panel gives a user a set of options for accessing control points.
   There are currently five opitions avilable to a user.

   - **Next**: Go to the next control points
   - **Previous**: Go to previous control point
   - **First**: Jump to first control point
   - **Last**: Jump to last control point
   - **Random access** box: Directly go to any control points.

**3) Save the points:**

Before moving on to the next ste values for control points please make sure to add/edit values
by pressing green add button in Figure 3.

**4) Add more control points**

Add as many control points as needed.

**5) Save and generate XML beam**

After having added the control points please press the `Generate XML` button and a final XML beam
will be genrated. The `Generated XML` window in Figure 1 shows that user gets a first glimpse of the
file.

.. Note::

   The file can also accessed via `Open XML file` option as show in the figure 1.

   For sanity check user can plot the generated XML file

--------------------------------------------------------------------------------------------------------

Adding imaging points
---------------------
**During treatment images**

In treatment imaging captures images concurrent with the MV therapeutic beam.
Logically, imaging points are specified as MU vs. position trajectory.
A user is presented with a screen with an option to choose from kV or MV image as shown
in Figure 5

**1) Fill in kV or MV parameters**

.. figure:: images/kVimagingWindow.png
   :scale: 75 %

   Figure (5) Main imaging window with the option of insert kV as well MV images

User can add imaging points and proceed to next point.

**2) Choose image modes**

Current version gives user 4 kV and 6 MV image mode options.
These are shown in figures 6 and 7.

|

.. figure:: images/kvImageMode.png
   :scale: 90 %

   Figure (6) Image modes avilable for kV imaging

|

.. figure:: images/MVImageMode.png
   :scale: 90 %

   Figure (7) Image modes avilable for MV imaging

**3) Finish entering imaging points**

After having add all the imaging points user can press the done button.

-------------------------------------------------------------------------------------------------------

Modify an existing XML beam
---------------------------
- To modify an existing XML user can choose a beam through open beam option and
  then modify different control point values.

- The interface for this section is same as in section 2.

- Imaging points can also inserted as described in section 3.

--------------------------------------------------------------------------------------------------------

Convert DICOM-RT to XML
-----------------------

- To convert a DICOM-RT file to the XML beam a user can pick a file by choosing
  dcm2xml button.

- Once the XML beam is generated, it can modified as described in the previous section.

-------------------------------------------------------------------------------------------------------

Extra functions
---------------

Plot functions
++++++++++++++

Currently, MU vs Gantry Rotation function can be plotted for a given XML beam.
A typical XML plot is shown in Figure (8)


.. figure:: images/MUvsGantry.png
   :scale: 80 %

   Figure (8) An example of MU vs Gantry rotation plot

XML beam in a text editor
+++++++++++++++++++++++++
A typical XML file generated by Veritas can be accessed through a text editor (shown in Notepad++).
The text editor functionality gives the user flexibility to control the generated XML file by hand.


.. figure:: images/Output_notepad.png
   :scale: 90 %

   Figure (9) An XML file shown in a text editor (shown in Notepad++)
