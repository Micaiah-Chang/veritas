Citing Varian Veritas
---------------------

Please acknowledge this work by citing `our work <http://amos3.aapm.org/disposition.php?abstractID=23602>`_
