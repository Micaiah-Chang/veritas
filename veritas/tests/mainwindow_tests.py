import sys, os, glob, shutil, tempfile
from nose.tools import assert_equal

from PySide.QtGui import QApplication
from PySide.QtTest import QTest
from PySide.QtCore import Qt

from mainWindow import MainWindow
import beamon
from imaging import cpImage
# Modules to test

from utils.SetBeam20 import VarianResearchBeam

app = QApplication(sys.argv)

TEST_DIR = "tests"

EXAMPLE_DIR = "examples"
TEST_OUTPUT_DIR = "test_output"
TEST_INPUT_DIR = "input"

NOSETEST_OUTPUT_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "output.xml")
VERITAS_OUTPUT_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "output.xml")

TEST_VEL_TOL_OUTPUT_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "vel_tol.xml")
VEL_TOL_OUTPUT_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "vel_tol.xml")

NOSETEST_IMAGE_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "imaging.xml")
VERITAS_IMAGE_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "imaging.xml")

IMAGE_INPUT_PATH = os.path.join(TEST_DIR, TEST_INPUT_DIR, "image_input.xml")

NOSETEST_MULTI_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "multi_cp.xml")
VERITAS_MULTI_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "multi_cp.xml")

CP1_FIELDS = [("Mu", "0"), ("GantryRtn", "10.0"), ("CollRtn", "1.6"), ("CouchVrt", "-5.4"),
              ("CouchLat", "150.0"), ("CouchLng", "-2.7"), ("CouchRtn", "0.1"),
              ("X1", "-16.0"), ("X2", "-14.3"), ("Y1", "14.0"), ("Y2", "4.0"),]

CP2_FIELDS = [("Mu", "2.0")]

CP3_FIELDS = [("Mu", "3.0")]

TOL_FIELDS = [("GantryRtn", "10.0"), ("CollRtn", "1.6"), ("CouchVrt", "-5.4"),
              ("CouchLat", "150.0"), ("CouchLng", "-2.7"), ("CouchRtn", "0.1"),
              ("X1", "-16.0"), ("X2", "-14.3"), ("Y1", "14.0"), ("Y2", "4.0"),]

VEL_FIELDS = [("GantryRtn", "2"), ("CollRtn", "2"), ("CouchVrt", "2"), ("CouchLat", "2"),
              ("CouchLng", "2"), ("CouchRtn", "2"), ("X1", "2"), ("X2", "2"),
              ("Y1", "2"), ("Y2", "2"),]

IMAGE_FIELD1 = {"QLineEdit" :
                {"kvAxisValue": "0.5",
                 "kvdLat": "1", "kvdLng": "2", "kvdVrt": "3", "kvdPitch": "4",
                 "kvsLat": "5", "kvsLng": "6", "kvsVrt": "7", "kvsPitch": "8",
                 "kvShape": "1", "kvFoil": "1"},
                "QSpinBox": {"KiloVolts": "45", "MilliAmperes": "25", "MilliSeconds": "15"},
                "QLabel" : {"kvImageModeLabel": 'DynamicGain'}}

IMAGE_FIELD2 = {"QLineEdit": {"kvAxisValue": "1.5"}}

MLC_MAPPINGS = {"NDS80": 0, "NDS120": 1, "NDS120HD": 2}

# NOTE: VEL TOL HAS TO COME BEFORE BEAMON DUE TO MUTABLE DEFAULTS!!!

def test_main():
    mw = MainWindow()
#    sys.exit(app.exec_())


def test_beamon():
    header = beamon.beamonHeader()


    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS80"))

    ui_field_helper(header, "DRate", "62.0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "2")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    form = header.cpWindow
    batch_field_set(form, CP1_FIELDS)
    # Need DRate and Energy as well as subbeam

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_vel_tol():
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS80"))

    ui_field_helper(header, "DRate", "62.0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "2")

    vel_table = header.velTable

    batch_field_set(vel_table, VEL_FIELDS)

    vel_widget = vel_table.buttonBox.button(vel_table.buttonBox.Ok)
    vel_widget.click()
    QTest.mouseClick(vel_widget, Qt.LeftButton)

    header.velTableProperties = header.velTable.tableVal

    tol_table = header.tolTable

    batch_field_set(tol_table, TOL_FIELDS)

    tol_widget = tol_table.buttonBox.button(tol_table.buttonBox.Ok)
    tol_widget.click()
    QTest.mouseClick(tol_widget, Qt.LeftButton)

    header.tolTableProperties = header.tolTable.tableVal

    ok_widget = header.buttonBox.button(header.buttonBox.Ok)
    ok_widget.click()
    QTest.mouseClick(ok_widget, Qt.LeftButton)

    header.accept()
    form = header.cpWindow
    batch_field_set(form, CP1_FIELDS)

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()
    cp_xml_generate(form, TEST_VEL_TOL_OUTPUT_PATH)

def test_multiple_beamon():
    header = beamon.beamonHeader()

    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS80"))

    ui_field_helper(header, "DRate", "62.0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "2")

    header.accept()

    def add_and_nav(form, fields):
        batch_field_set(form, fields)
        form.editAddButton.click()

    form = header.cpWindow
    add_and_nav(form, CP1_FIELDS)
    add_and_nav(form, CP2_FIELDS)
    add_and_nav(form, CP3_FIELDS)

    form.doneButton.click()

    cp_xml_generate(form, NOSETEST_MULTI_PATH)

def test_imaging():
    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.selectKV.click()

    image_fields_helper(cp_image, IMAGE_FIELD1)
    cp_image.nextButton.click()

    image_fields_helper(cp_image, IMAGE_FIELD2)
    cp_image.nextButton.click()

    # Not a click so that no user input is required
    cp_image.doneCP(NOSETEST_IMAGE_PATH)


def compare_xml_output(test_output, control_output):
    with open(test_output, "r") as f:
        export_xml = f.read()

    with open(control_output, "r") as f:
        output_xml = f.read()

    assert_equal(output_xml, export_xml, "Not equal")


def test_beamon_xml():
    compare_xml_output(NOSETEST_OUTPUT_PATH, VERITAS_OUTPUT_PATH)

def test_vel_tol_xml():
    compare_xml_output(VEL_TOL_OUTPUT_PATH, TEST_VEL_TOL_OUTPUT_PATH)


def test_multi_xml():
    compare_xml_output(NOSETEST_MULTI_PATH, VERITAS_MULTI_PATH)

def test_imaging_xml():
    compare_xml_output(NOSETEST_IMAGE_PATH, VERITAS_IMAGE_PATH)

def ui_field_helper(form, field, value):
    field_name = getattr(form, field)
    field_name.setText(value)
    assert_equal(field_name.text(), value,
                 "form.{0} is {1}".format(field, field_name.text()))

def set_spinboxes(form, field_pairs):
    for name, value in field_pairs.items():
        field_name = getattr(form, name)
        field_name.setValue(int(value))

def set_labels(form, field_pairs):
    for name, value in field_pairs.items():
        field_name = getattr(form, name)
        field_name.setText(value)



def image_fields_helper(image, field_dict):
    for field_type, field_pairs in field_dict.items():
        if field_type == "QLineEdit":
            batch_field_set(image, tuple(field_pairs.items()))
        elif field_type == "QSpinBox":
            set_spinboxes(image, field_pairs)
        elif field_type == "QLabel":
            set_labels(image, field_pairs)


def batch_field_set(ui_window, zipped_forms):
    map(lambda x: ui_field_helper(ui_window, x[0], x[1]), zipped_forms)

def cp_xml_generate(form, output_file):
    cpdata = form.cpdata
    root = cpdata.create_xml()

    root_tag = 'VarianResearchBeam'
    root_obj = VarianResearchBeam.factory()
    root_obj.build(root)

    out_file = open(output_file, "w")
    root_obj.export(out_file, 0, name_=root_tag,
                    namespacedef_='', pretty_print=True)
